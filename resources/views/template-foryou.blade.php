{{--
  Template Name: For You Template
--}}

@extends('layouts.app')

@section('content')
  <section id="foryoufold" class="uk-block-xlarge bg-white">
    <div class="gridl">
      <div class="uk-grid-small" uk-grid>
        <div id="foryoufold-left" class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
          <h1 class="bold black">Is this program for you?</h1>
          <p class="black">Lorem ipsum dolor sit amet, consectetuer
              adipiscing elit, sed diam nonummy nibh euismod
              tincidunt ut laoreet dolore magna aliquam
              erat volutpat. Ut wisi enim ad minim veniam.</p>
          <a href="#" class="button button-green">Call Now</a>
        </div>
        <div id="foryoufold-right" class="uk-text-center uk-text-right@m uk-width-1-1 uk-width-1-2@m">
          <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-For-You-Overlay-500x500.png">
        </div>
      </div>
    </div>
  </section>

  <section id="foryou-description" class="uk-block-xlarge bg-white">
      <svg id="foryoutopsquiggle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 489.94 407.23"><defs><style>.cls-1ts{opacity:0.1;}.cls-2ts{fill:#00953b;}</style></defs><title>Asset 7</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-1ts"><path class="cls-2ts" d="M317.69,172c-5.25,3.62-3.68,2.65-1.93,1.44.6-.46,1.2-.91,1.81-1.34Z"/><path class="cls-2ts" d="M489.91,30.32c-.74-16.47-13.18-29.73-30-30C408.48-.5,356.42-.59,306,11a263.85,263.85,0,0,0-34.5,10.38c-13,4.94-27.15,10.25-38.68,18.24-15.6,10.8-27.84,25.15-29.24,44.82-1.33,18.54,10.16,35.34,25.8,44.29,13.65,7.82,30.06,11.89,44.86,17,12.48,4.31,25.23,8.2,37.55,13,4.55,2.2,8.73,4.78,13,7.37-.42.37-.82.76-1.25,1.12-1.93,1.62-3.92,3.19-5.9,4.75l.3-.21-.42.31-.32.25c-.37.29-.94.7-1.49,1.09-1.05.8-2.09,1.62-3.15,2.41-9.9,7.35-20.08,14.34-30.45,21-21,13.57-42.83,25.89-64.89,37.67C172.73,258.3,127,279.91,83.4,305.39A665.64,665.64,0,0,0,8.79,356C3.16,360.39,0,370.44,0,377.2c0,7.36,3.27,16.15,8.79,21.22C14.5,403.66,22,407.56,30,407.2c7.79-.35,13.58-3.1,19.6-7.56l-.46.36.53-.41c.51-.38,1-.77,1.54-1.17l.12-.1.49-.36q2.7-2,5.41-4,5.43-4,11-7.85,11-7.69,22.44-14.89c15.21-9.62,30.79-18.63,46.57-27.29,31.43-17.24,63.61-33.08,95.38-49.67s63.45-34.09,93.18-54.41q10.77-7.35,21.18-15.23c8.27-6.28,17.57-12.54,24.72-20.18,17.26-18.43,22.41-46.52,7.06-67.93-14-19.54-34.65-30-56.87-37.65C307,93.69,291.8,89,276.9,83.74q2.78-1.3,5.57-2.53c21.14-8.51,43.29-13.9,65.8-17.24,37-4.58,74.4-4.25,111.64-3.65C475.61,60.57,490.63,46.34,489.91,30.32ZM325,166l-.11.11C321.54,164.06,323.17,164.75,325,166Z"/></g></g></g></svg>
      
      <svg id="foryoumidsquiggle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 311.14 345.43"><defs><style>.cls-1ts{opacity:0.1;}.cls-2ts{fill:#00953b;}</style></defs><title>Asset 8</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-1ts"><path class="cls-2ts" d="M311.12,315.38c-.77-17.21-13.2-28.82-30-30q-21.3-1.51-42.47-4.23-35.18-5-69.75-13.28c-21.83-5.28-43.87-11.09-64.79-19.43a156.09,156.09,0,0,1-14-7c-2.68-1.54-5.3-3.16-7.86-4.9-1.38-.94-2.71-2-4.11-2.92l1,.67-.24-.19a85.2,85.2,0,0,1-6.33-6c-.91-1-1.79-1.91-2.65-2.89-.69-.79-1.36-1.6-2-2.41l2.76,3.59C69,226,66.38,220,65.5,218.51s-1.81-3.29-2.64-5A72.65,72.65,0,0,1,60.19,204a61.83,61.83,0,0,1,0-6.67A57.77,57.77,0,0,1,62,190.68q1.59-3.19,3.48-6.26c.87-1.39,1.77-2.74,2.71-4.08.47-.67,1-1.33,1.43-2,2.42-3.39-1.49,1.49-1.68,2.17.72-2.6,5.22-6.17,7-8.11,2.43-2.65,4.91-5.25,7.41-7.83q15.24-15.66,31.93-29.81,4.17-3.54,8.41-7l3.94-3.14.35-.29c5.67-4.27,11.39-8.47,17.23-12.51Q162.55,99.19,182,88.32q20.15-11.23,41.32-20.39,11.24-4.61,22.68-8.67c7.44-2.63,13.84-6.8,17.93-13.79,3.76-6.43,5.35-15.9,3-23.11-4.71-14.57-21.06-26.55-36.9-21C155.43,27.78,85.34,71.67,32,130.58,15.08,149.26,2.51,169.79.27,195.35-1.57,216.28,6.12,239.23,18.43,256a134.94,134.94,0,0,0,51.14,42.29c19.87,9.49,40.85,16.09,62,21.91a771.18,771.18,0,0,0,149.52,25.16C296.8,346.48,311.81,330.78,311.12,315.38Z"/><path class="cls-2ms" d="M79.19,234.26c1.73,1.39,5.32,3.5,0,0Z"/></g></g></g></svg>

      <svg id="foryoubotsquiggle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 224.94 243.52"><defs><style>.cls-1bs{fill:#00953b;opacity:0.1;}</style></defs><title>Asset 9</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1bs" d="M220.36,157.75a169.93,169.93,0,0,0-24.23-53c-23.34-34-56.35-59.44-93.22-77.41-21-10.25-43-18.66-65.14-26.08C22.88-3.75,4.33,7,.87,22.19c-3.77,16.57,5,31.57,21,36.9C34,63.18,46.16,67.56,58.08,72.45a325.3,325.3,0,0,1,31.86,15.8q8.84,5,17.25,10.78c2.43,1.66,4.8,3.41,7.18,5.13,1.22,1,2.42,2,3.62,3a180.42,180.42,0,0,1,13.88,12.93q3.33,3.43,6.43,7.08c.86,1,1.7,2,2.53,3a169,169,0,0,1,16.75,28.37,135.19,135.19,0,0,1,7.06,25.34,115.56,115.56,0,0,1,.07,21.53c0,.15-.05.31-.08.46-1.4,8.15-1.29,15.74,3,23.12,3.64,6.22,10.77,12.16,17.93,13.79,14.56,3.31,34-4.42,36.9-21C226.22,200.25,225.84,179,220.36,157.75Z"/></g></g></svg>
      
    <div class="gridl bg-gray">
      <div class="grids">
        <div class="uk-grid-small" uk-grid>
          <div class="text uk-text-center uk-text-left@m uk-width-1-1 uk-width-1-2@m">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.6 41.2"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 3</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M20.49.44A.85.85,0,0,0,19.74,0H6a.85.85,0,0,0-.83.65L0,21.25a.85.85,0,0,0,.83,1.07H6.6L1.75,40.12a.87.87,0,0,0,.47,1,.86.86,0,0,0,1.07-.31L20.46,15.07a.87.87,0,0,0-.72-1.34h-7L20.47,1.31A.83.83,0,0,0,20.49.44Z"/></g></g></svg>
            <h2 class="bold black">Description 1</h2>
            <p class="black">Lorem ipsum dolor sit amet, consectetuer
              adipiscing elit, sed diam nonummy nibh euismod
              tincidunt ut laoreet dolore magna aliquam
              erat volutpat. Ut wisi enim ad minim veniam.</p>
          </div>
          <div class="image uk-text-center uk-text-right@m uk-width-1-1 uk-width-1-2@m">
            <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-For-You-water-500x500.png">
          </div>
          <div class="image uk-text-center uk-text-left@m uk-width-1-1 uk-width-1-2@m">
            <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-For-You-water-500x500.png">
          </div>
          <div class="text uk-text-center uk-text-left@m uk-width-1-1 uk-width-1-2@m">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.6 41.2"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 3</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M20.49.44A.85.85,0,0,0,19.74,0H6a.85.85,0,0,0-.83.65L0,21.25a.85.85,0,0,0,.83,1.07H6.6L1.75,40.12a.87.87,0,0,0,.47,1,.86.86,0,0,0,1.07-.31L20.46,15.07a.87.87,0,0,0-.72-1.34h-7L20.47,1.31A.83.83,0,0,0,20.49.44Z"/></g></g></svg>
            <h2 class="bold black">Description 2</h2>
            <p class="black">Lorem ipsum dolor sit amet, consectetuer
              adipiscing elit, sed diam nonummy nibh euismod
              tincidunt ut laoreet dolore magna aliquam
              erat volutpat. Ut wisi enim ad minim veniam.</p>
          </div>
          <div class="text uk-text-center uk-text-left@m uk-width-1-1 uk-width-1-2@m">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.6 41.2"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 3</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M20.49.44A.85.85,0,0,0,19.74,0H6a.85.85,0,0,0-.83.65L0,21.25a.85.85,0,0,0,.83,1.07H6.6L1.75,40.12a.87.87,0,0,0,.47,1,.86.86,0,0,0,1.07-.31L20.46,15.07a.87.87,0,0,0-.72-1.34h-7L20.47,1.31A.83.83,0,0,0,20.49.44Z"/></g></g></svg>
            <h2 class="bold black">Description 3</h2>
            <p class="black">Lorem ipsum dolor sit amet, consectetuer
              adipiscing elit, sed diam nonummy nibh euismod
              tincidunt ut laoreet dolore magna aliquam
              erat volutpat. Ut wisi enim ad minim veniam.</p>
          </div>
          <div class="image uk-text-center uk-text-right@m uk-width-1-1 uk-width-1-2@m">
            <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-For-You-water-500x500.png">
          </div>
          <div class="image uk-text-center uk-text-left@m uk-width-1-1 uk-width-1-2@m">
            <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-For-You-water-500x500.png">
          </div>
          <div class="text uk-text-center uk-text-left@m uk-width-1-1 uk-width-1-2@m">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.6 41.2"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 3</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M20.49.44A.85.85,0,0,0,19.74,0H6a.85.85,0,0,0-.83.65L0,21.25a.85.85,0,0,0,.83,1.07H6.6L1.75,40.12a.87.87,0,0,0,.47,1,.86.86,0,0,0,1.07-.31L20.46,15.07a.87.87,0,0,0-.72-1.34h-7L20.47,1.31A.83.83,0,0,0,20.49.44Z"/></g></g></svg>
            <h2 class="bold black">Description 4</h2>
            <p class="black">Lorem ipsum dolor sit amet, consectetuer
              adipiscing elit, sed diam nonummy nibh euismod
              tincidunt ut laoreet dolore magna aliquam
              erat volutpat. Ut wisi enim ad minim veniam.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection