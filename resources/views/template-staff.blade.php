{{--
  Template Name: Staff Template
--}}

@extends('layouts.app')

@section('content')
  <section id="stafffold" class="uk-block-xlarge bg-white">
    <div class="gridl">
      <div class="uk-grid-small" uk-grid>
        <div id="stafffold-left" class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
          <h1 class="bold black">Our physicians are experts
in their field.</h1>
          <p class="black">Lorem ipsum dolor sit amet, consectetuer
              adipiscing elit, sed diam nonummy nibh euismod
              tincidunt ut laoreet dolore magna aliquam
              erat volutpat. Ut wisi enim ad minim veniam.</p>
          <a href="#" class="button button-green">See Facility</a>
        </div>
        <div id="stafffold-right" class="uk-text-center uk-text-right@m uk-width-1-1 uk-width-1-2@m">
          <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-Staff-Overlay-500x500.png">
        </div>
      </div>
    </div>
  </section>

  <section id="belowfold" class="uk-block-xlarge bg-white">
    <svg id="stafftopsquiggle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 446.03 1033.16"><defs><style>.cls-1{opacity:0.1;}.cls-2{fill:#0033a1;}</style></defs><title>Asset 18</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-1"><path class="cls-2" d="M207.5,142.62l-.33-.25c5,4,3.13,2.46,1.41,1.09-.34-.25-.66-.52-1-.77Z"/><path class="cls-2" d="M429.76,355.72c-22.56-79-68.65-150.36-126.39-208.28A630.44,630.44,0,0,0,207.15,69C170.6,44.51,130.93,21.86,88.92,8.37A203.69,203.69,0,0,0,56.42.75c-8.3-1.23-15.58-1.39-23.12,3C27.08,7.41,21.14,14.55,19.51,21.7c-1.75,7.68-1.37,16.3,3,23.12,3.93,6.1,10.4,12.67,17.93,13.78,17.65,2.61,34.66,7.79,51.16,14.46,24.09,10.56,47.19,23.53,69.45,37.48q16.6,10.41,32.55,21.83,6.83,4.9,13.54,10l-.77-.62c.39.32.79.63,1.19.94l1,.77,1.52,1.19q4.31,3.4,8.55,6.88a563.06,563.06,0,0,1,55.82,52.39q12.69,13.7,24.4,28.28c1.53,1.92,8.17,10.94,2,2.49,1.83,2.51,3.76,4.95,5.6,7.45q6,8.13,11.62,16.5a450.56,450.56,0,0,1,39.33,71.54,397.87,397.87,0,0,1,25.5,93.18,390.55,390.55,0,0,1,.14,94.5,351,351,0,0,1-21.75,80.52,314.44,314.44,0,0,1-19.44,37.48Q335.68,646,328.68,655.7c-1,1.35-2,2.7-3,4-3,3.65-5.94,7.31-9,10.87-24.2,27.91-52.63,51.69-80.81,75.41l-91.79,77.22c-29.25,24.61-59.46,48.42-87.63,74.27C26.34,925.14-.22,960.44,0,1003.13c.08,15.69,13.74,30.73,30,30s30.09-13.18,30-30c0-2.26.08-4.49.24-6.73a85.58,85.58,0,0,1,3.7-13.15c1.43-3,3-5.9,4.67-8.74q2-3.25,4.1-6.38c.85-1.24,3.11-4.09,3.59-4.72a244.23,244.23,0,0,1,17.38-18.6c16.22-15.55,33.92-29.58,51.1-44L258.57,805c35.37-29.76,72.18-58.76,102.3-94.06a349.68,349.68,0,0,0,64.22-112.17C452.18,521.44,452.18,434.22,429.76,355.72Z"/></g></g></g></svg>
    <div id="teamblock" class="gridl bg-gray shadow">
      <div class="uk-block">
        <div class="header uk-text-center">
          <h2 class="bold black">Our Team</h2>
        </div>
        <div class="body uk-text-center">
          <div class="uk-grid-small staffgrid" uk-grid>
            <div class="uk-width-1-1 uk-width-1-3@m">
              <div class="single uk-text-left">
                <div class="image uk-text-center"> 
                  <img src="/cpe/wp-content/uploads/2019/04/Staff-member-1-500x500.png">
                </div>
                <h3 class="bold black">Name Here</h3>
                <p class="black">Title</p>
                <p class="black bold">Bio</p>
                <p class="black">Bio here Lorem ipsum dolor sit amet,
                    consectetuer adipiscing elit, sed diam
                    nonummy nibh euismod tincidunt ut
                    laoreet dolore magna aliquam erat
                    volutpat. Ut wisi enim ad minim veniam,
                    quis nostrud exerci tation ullamcorper .</p>
              </div>
            </div>
            <div class="uk-width-1-1 uk-width-1-3@m">
              <div class="single uk-text-left">
                <div class="image uk-text-center"> 
                  <img src="/cpe/wp-content/uploads/2019/04/Staff-member-2-500x500.png">
                </div>
                <h3 class="bold black">Name Here</h3>
                <p class="black">Title</p>
                <p class="black bold">Bio</p>
                <p class="black">Bio here Lorem ipsum dolor sit amet,
                    consectetuer adipiscing elit, sed diam
                    nonummy nibh euismod tincidunt ut
                    laoreet dolore magna aliquam erat
                    volutpat. Ut wisi enim ad minim veniam,
                    quis nostrud exerci tation ullamcorper .</p>
              </div>
            </div>
            <div class="uk-width-1-1 uk-width-1-3@m">
              <div class="single uk-text-left">
                <div class="image uk-text-center"> 
                  <img src="/cpe/wp-content/uploads/2019/04/Staff-member-3-500x500.png">
                </div>
                <h3 class="bold black">Name Here</h3>
                <p class="black">Title</p>
                <p class="black bold">Bio</p>
                <p class="black">Bio here Lorem ipsum dolor sit amet,
                    consectetuer adipiscing elit, sed diam
                    nonummy nibh euismod tincidunt ut
                    laoreet dolore magna aliquam erat
                    volutpat. Ut wisi enim ad minim veniam,
                    quis nostrud exerci tation ullamcorper .</p>
              </div>
            </div>
            <div class="uk-width-1-1 uk-width-1-3@m">
              <div class="single uk-text-left">
                <div class="image uk-text-center"> 
                  <img src="/cpe/wp-content/uploads/2019/04/Staff-member-4-500x500.png">
                </div>
                <h3 class="bold black">Name Here</h3>
                <p class="black">Title</p>
                <p class="black bold">Bio</p>
                <p class="black">Bio here Lorem ipsum dolor sit amet,
                    consectetuer adipiscing elit, sed diam
                    nonummy nibh euismod tincidunt ut
                    laoreet dolore magna aliquam erat
                    volutpat. Ut wisi enim ad minim veniam,
                    quis nostrud exerci tation ullamcorper .</p>
              </div>
            </div>
            <div class="uk-width-1-1 uk-width-1-3@m">
              <div class="single uk-text-left">
                <div class="image uk-text-center"> 
                  <img src="/cpe/wp-content/uploads/2019/04/Staff-member-5-500x500.png">
                </div>
                <h3 class="bold black">Name Here</h3>
                <p class="black">Title</p>
                <p class="black bold">Bio</p>
                <p class="black">Bio here Lorem ipsum dolor sit amet,
                    consectetuer adipiscing elit, sed diam
                    nonummy nibh euismod tincidunt ut
                    laoreet dolore magna aliquam erat
                    volutpat. Ut wisi enim ad minim veniam,
                    quis nostrud exerci tation ullamcorper .</p>
              </div>
            </div>
            <div class="uk-width-1-1 uk-width-1-3@m">
              <div class="single uk-text-left">
                <div class="image uk-text-center"> 
                  <img src="/cpe/wp-content/uploads/2019/04/Staff-member-6-500x500.png">
                </div>
                <h3 class="bold black">Name Here</h3>
                <p class="black">Title</p>
                <p class="black bold">Bio</p>
                <p class="black">Bio here Lorem ipsum dolor sit amet,
                    consectetuer adipiscing elit, sed diam
                    nonummy nibh euismod tincidunt ut
                    laoreet dolore magna aliquam erat
                    volutpat. Ut wisi enim ad minim veniam,
                    quis nostrud exerci tation ullamcorper .</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="staff-committed" class="uk-block-xlarge bg-white">
    <div class="gridl">
      <div class="uk-grid-collapse" uk-grid>
        <div id="staff-committed-left" class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-right@m">
          <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-Staff-bottom-608x424.png">
        </div>
        <div id="staff-committed-right" class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
          <h2 class="bold black">We're committed to your long-term sobriety.</h2>
          <p class="black">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
              nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
              volutpat. Ut wisi enim ad minim veniam.</p>
        </div>
      </div>
      <div class="blackline-center-100"></div>
      <div id="staff-blurbs" class="uk-grid-small" uk-grid>
        <div class="uk-width-1-1 uk-width-1-3@m">
          <div class="single">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1a{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1a" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1a" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
            <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
            <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
            <a href="#" class="green">Learn More</a>
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-3@m">
          <div class="single">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1a{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1a" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1a" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
            <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
            <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
            <a href="#" class="green">Learn More</a>
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-3@m">
          <div class="single">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1a{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1a" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1a" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
            <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
            <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
            <a href="#" class="green">Learn More</a>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection