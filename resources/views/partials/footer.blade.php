<footer class="content-info">
  <?php if(is_page(12)): ?>
  <div class="ctacontainer bg-gray uk-block">
  <?php else: ?>
  <div class="ctacontainer bg-white uk-block">
  <?php endif; ?>
    <div class="cta gridm bg-white">
      <div class="uk-grid-small" uk-grid>
        <div id="footercta-left" class="uk-width-1-1 uk-width-1-2@m">
          <h2 class="bold black">Get the best treatment for yourself or a loved one</h2>
        </div>
        <div id="footercta-right" class="uk-width-1-1 uk-width-1-2@m">
          <?= do_shortcode('[contact-form-7 id="34" title="threefields"]'); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="regfooter bg-dark">
    <div class="gridl">
      <div class="uk-block-xlarge">
        <a href="https://journeypure.com" target="_blank">
          <img src="/cpe/wp-content/uploads/2019/04/A-JourneyPure-Facility-white-1000x389.png" alt="A JourneyPure Facility">
        </a>
        <div class="uk-block-small">
          <div class="uk-grid-small" uk-grid>
            <div class="uk-width-1-1 uk-width-1-3@m">
              <h3 class="bold white">The Standard in Addiction Treatment</h3>
              <p class="white">&copy; <?= date('Y'); ?> JourneyPure</p>
            </div>
            <div class="uk-width-1-1 uk-width-1-3@m">
              <h3 class="bold white">Contact</h3>
              <p class="white">JourneyPure CPE</p>
              <p class="white">5080 Florence Road</p>
              <p class="white">Murfreesboro, TN 37129</p>
              <a href="#" class="bold white">PHONE NUMBER</a>
            </div>
            <div class="uk-width-1-1 uk-width-1-3@m">
              <h3 class="bold white">Navigation</h3>
              <div class="uk-grid-small footer-nav-grid" uk-grid>
                <div class="uk-width-1-2">
                  <a href="#">Nav Link 1</a>
                  <a href="#">Nav Link 2</a>
                </div>
                <div class="uk-width-1-2">
                  <a href="#">Nav Link 3</a>
                  <a href="#">Nav Link 4</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
