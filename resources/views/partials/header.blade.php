<header class="banner">
  <div class="container gridl">
    <div class="uk-grid-collapse" uk-grid>
      <div class="uk-width-1-1 uk-width-1-4@m">
          <a class="brand" href="{{ home_url('/') }}">
            <img src="/cpe/wp-content/uploads/2019/04/JP_CPE-01.png" />
          </a>
      </div>
      <div id="nav-block" class="uk-width-1-1 uk-width-3-4@m">
        <nav class="nav-primary">
          @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
          @endif
        </nav>
      </div>
    </div>
  </div>
  <a href="#offcanv" id="offcanv-toggle" uk-toggle>
    <span uk-icon="icon: menu"></span>
  </a>
  <div id="offcanv" uk-offcanvas="flip: true">
    <div class="uk-offcanvas-bar">
      <button class="uk-offcanvas-close" type="button" uk-close></button>
      <nav id="offcanv-nav" class="nav-mobile">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif
      </nav>
      <a href="#" class="green bold">Call Now<br>(888) 888-8888</a>
    </div>
  </div>
</header>

