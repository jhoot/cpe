{{--
  Template Name: Location Template
--}}

@extends('layouts.app')

@section('content')
  <section id="locationfold" class="uk-block-xlarge bg-white">
    <div class="gridl">
      <div class="uk-grid-small" uk-grid>
        <div id="locationfold-left" class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
          <h1 class="bold black">CPE Location</h1>
          <p class="black">Lorem ipsum dolor sit amet, consectetuer
              adipiscing elit, sed diam nonummy nibh euismod
              tincidunt ut laoreet dolore magna aliquam
              erat volutpat. Ut wisi enim ad minim veniam.</p>
          <a href="#" class="button button-green">See Facility</a>
        </div>
        <div id="locationfold-right" class="uk-width-1-1 uk-width-1-2@m">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3230.808635182985!2d-86.4695911843834!3d35.92718382405034!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8864657855d08e9f%3A0xdab7a92753451289!2sExecutive+Addiction+Recovery+Center+(JourneyPure+Center+for+Professional+Excellence)!5e0!3m2!1sen!2sus!4v1555509135888!5m2!1sen!2sus" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </section>

  <section class="uk-block-large bg-white" id="locationbelowfold">
    <div class="gridl">
      <div class="header uk-text-center">
        <h2 class="black bold">CPE Location Features</h2>
      </div>
      <div class="uk-block-small">
        <div class="uk-grid-small" uk-grid>
          <div class="single-feature uk-width-1-1 uk-width-1-3@m">
            <div class="single">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
              <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
              <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
              <a href="#" class="green">Learn More</a>
            </div>
          </div>
          <div class="single-feature uk-width-1-1 uk-width-1-3@m">
            <div class="single">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
              <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
              <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
              <a href="#" class="green">Learn More</a>
            </div>
          </div>
          <div class="single-feature uk-width-1-1 uk-width-1-3@m">
            <div class="single">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
              <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
              <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
              <a href="#" class="green">Learn More</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg id="facilitysquiggle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 689.91 316.02"><defs><style>.cls-1f{opacity:0.1;}.cls-2f{fill:#0033a1;}</style></defs><title>Asset 10</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-1f"><path class="cls-2f" d="M681.42,264.87c-38.2-43.85-78.19-86.19-120.09-126.53C521.13,99.64,477.77,62.29,428,36.49,377.13,10.16,318.6-5.61,261.16,1.86,229.24,6,199.75,16.28,170.8,30.25a525.37,525.37,0,0,0-70.19,40.57C73.36,89.64,49,111.36,30.11,138.74,11.67,165.46.46,197.62,0,230.18c-.22,15.7,14,30.72,30,30,16.45-.73,29.76-13.18,30-30q.07-5.27.59-10.48a136.59,136.59,0,0,1,7.23-25.86,160.05,160.05,0,0,1,10.46-19.32q3.09-4.87,6.5-9.53l.46-.62c1.36-1.63,2.7-3.28,4.09-4.89a214.21,214.21,0,0,1,17.93-18.23q5.08-4.6,10.42-8.92c.72-.58,2.32-1.68,3.36-2.38l-2.68,1.76c1.66-1.09,3.21-2.42,4.82-3.6a475.4,475.4,0,0,1,90.84-52A239.1,239.1,0,0,1,265.7,61.31a223.23,223.23,0,0,1,49.9.22,281.17,281.17,0,0,1,62.14,17.31,365.63,365.63,0,0,1,57,31.55q6.59,4.41,13,9.06,3.21,2.33,6.39,4.69l3.46,2.61c8.64,6.84,17.14,13.83,25.45,21.06,35.42,30.82,68.51,64.49,100.73,98.62Q612,276.29,639,307.3c10.34,11.87,32,11.38,42.43,0C693,294.64,692.45,277.54,681.42,264.87Z"/><path class="cls-2" d="M121,130c1.52-1,1-.7,0,0Z"/></g></g></g></svg>
  </section>
  
  <section class="uk-block-large bg-gray" id="locationgrid">
    <div class="gridl">
      <div class="uk-grid-medium" uk-grid>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-4@m">
          <div class="single-image">
            <img src="http://placekitten.com/300/300" alt="placeholder">
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection