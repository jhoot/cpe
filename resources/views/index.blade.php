@extends('layouts.app')

@section('content')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
  @endif
  <section id="index-header" class="uk-block-xlarge bg-white">
    <div class="header gridxs uk-text-center">
      <h1 class="black bold">Our Blog</h1>
      <p class="black">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
          nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
          volutpat. Ut wisi enim ad minim veniam.</p>
    </div>
    <svg id="indexsquiggle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542.48 196.15"><defs><style>.cls-1{opacity:0.1;}.cls-2{fill:#0033a1;}</style></defs><title>Asset 19</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-1"><path class="cls-2" d="M249.49,92.22l.14.17c-3.75-5.14-2.25-3-.92-1.19l.42.55C249.26,91.93,249.38,92.09,249.49,92.22Z"/><path class="cls-2" d="M538.66,71.88C535,65.66,527.89,59.73,520.73,58.1c-14.51-3.31-34.11,4.4-36.9,20.95a65.1,65.1,0,0,1-2.62,10.35c-.67,1.33-1.38,2.65-2.15,3.94a47.52,47.52,0,0,1-3.21,4.32c-2.68,2.88-5.43,5.61-8.42,8.27-.52.38-1.05.76-1.59,1.12-2.58,1.77-5.24,3.42-7.95,5-4.15,2.38-8.43,4.51-12.8,6.48-19.71,7.75-40.6,12.24-61.54,15.29-19.83,2.42-39.86,3.15-59.74,1a160.42,160.42,0,0,1-32.89-8.68c-3.43-1.59-6.8-3.3-10.08-5.2-2.14-1.23-4.23-2.52-6.29-3.88-1-.69-2.06-1.39-3.07-2.1-.81-.58-1.61-1.16-2.41-1.76l3.69,2.87c-4.47-2.77-8.57-7.34-12.19-11.09-3.86-4-7.47-8.22-10.94-12.56l.59.81c-.36-.49-.73-1-1.09-1.45l-.42-.55-.44-.58Q245.6,87,243,83.44c-3.33-4.58-6.62-9.2-10-13.75C225.11,59.05,217.05,48.34,207.57,39,172.82,5,122.52-6.74,75.53,3.67A136.17,136.17,0,0,0,8.64,40.62c-11,11.21-12,31.4,0,42.42s30.67,12,42.42,0c2.24-2.27,4.56-4.47,7-6.53l.32-.27c.94-.67,1.89-1.34,2.85-2a106.69,106.69,0,0,1,14.21-7.91,120.07,120.07,0,0,1,22-6,124.8,124.8,0,0,1,23.75,0,118.06,118.06,0,0,1,21.23,5.73,103.48,103.48,0,0,1,14.18,8c.44.3,1.35,1,2.1,1.52l.52.42a127.56,127.56,0,0,1,9.29,8.87c2.58,2.72,5,5.54,7.45,8.42.39.47.86,1,1.35,1.51,0,0,0,0,0,0A16,16,0,0,1,180,98.33a4.12,4.12,0,0,1-.54-.65l.76,1c4.84,6.49,9.5,13.12,14.31,19.63,10.65,14.44,21.66,28.93,35.8,40.16a165.2,165.2,0,0,0,49.47,27.33c30.35,10.59,64.18,12,95.95,8.89,33.47-3.32,66.14-9.84,97.07-23.23,32.08-13.89,62.77-40.32,68.86-76.45C543.06,86.83,543,79.29,538.66,71.88Z"/><path class="cls-2" d="M177.33,94.84h0c.15.2,1.41,1.88,2.15,2.85l-1.68-2.23Z"/></g></g></g></svg>
  </section>

  <section id="blogroll" class="uk-block bg-white">
    <div class="gridl">
      <div class="uk-grid-collapse" uk-grid>
        @while (have_posts()) @php the_post() @endphp
          <div class="uk-width-1-1 uk-width-1-3@m">
            <div class="single">
              <div class="image">
                <img src="<?php the_post_thumbnail_url(); ?>">
              </div>
              <div class="text">
                <h3 class="bold black"><?php the_title(); ?></h3>
                <p class="black"><?php the_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>" class="bold green">Read More</a>
              </div>
            </div>
          </div>
        @endwhile
      </div>
    </div>
  </section>
  

  {!! get_the_posts_navigation() !!}
@endsection
