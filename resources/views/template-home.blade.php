{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')
  <section id="homefold" class="uk-block-large">
    <div class="gridl">
      <div class="uk-grid-collapse" uk-grid>
        <div id="left-block" class="uk-width-1-1 uk-width-2-5@m uk-text-center uk-text-left@m">
          <h1 class="bold">You can do it.<br>We will show you how.</h1>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
          <a href="#" class="button button-green">Our Services</a>
        </div>
        <div id="right-block" class="uk-width-1-1 uk-width-3-5@m">
          <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-hero-photo-tile-1000x623.png">
        </div>
      </div>
    </div>
  </section>

  <section id="underhomefold" class="bg-gray uk-block-large">
    <div class="grids">
      <div class="uk-text-center header-text gridxs">
        <h2 class="bold">Is this program for you?</h2>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
        <span class="cta bold">Is this you? <a href="#" class="green">Learn More</a></span>
      </div>
      <div class="uk-block-small description-grid">
        <div class="uk-grid-small" uk-grid>
          <div class="uk-width-1-1 uk-width-1-2@m">
            <div class="single-block bg-white">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.6 41.2"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 3</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M20.49.44A.85.85,0,0,0,19.74,0H6a.85.85,0,0,0-.83.65L0,21.25a.85.85,0,0,0,.83,1.07H6.6L1.75,40.12a.87.87,0,0,0,.47,1,.86.86,0,0,0,1.07-.31L20.46,15.07a.87.87,0,0,0-.72-1.34h-7L20.47,1.31A.83.83,0,0,0,20.49.44Z"/></g></g></svg>
              <h3 class="bold black">Description 1</h3>
              <p class="black">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.</p>
            </div>
          </div>
          <div class="uk-width-1-1 uk-width-1-2@m">
            <div class="single-block bg-white">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.6 41.2"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 3</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M20.49.44A.85.85,0,0,0,19.74,0H6a.85.85,0,0,0-.83.65L0,21.25a.85.85,0,0,0,.83,1.07H6.6L1.75,40.12a.87.87,0,0,0,.47,1,.86.86,0,0,0,1.07-.31L20.46,15.07a.87.87,0,0,0-.72-1.34h-7L20.47,1.31A.83.83,0,0,0,20.49.44Z"/></g></g></svg>
              <h3 class="bold black">Description 2</h3>
              <p class="black">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.</p>
            </div>
          </div>
          <div class="uk-width-1-1 uk-width-1-2@m">
            <div class="single-block bg-white">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.6 41.2"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 3</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M20.49.44A.85.85,0,0,0,19.74,0H6a.85.85,0,0,0-.83.65L0,21.25a.85.85,0,0,0,.83,1.07H6.6L1.75,40.12a.87.87,0,0,0,.47,1,.86.86,0,0,0,1.07-.31L20.46,15.07a.87.87,0,0,0-.72-1.34h-7L20.47,1.31A.83.83,0,0,0,20.49.44Z"/></g></g></svg>
              <h3 class="bold black">Description 3</h3>
              <p class="black">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.</p>
            </div>
          </div>
          <div class="uk-width-1-1 uk-width-1-2@m">
            <div class="single-block bg-white">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.6 41.2"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 3</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M20.49.44A.85.85,0,0,0,19.74,0H6a.85.85,0,0,0-.83.65L0,21.25a.85.85,0,0,0,.83,1.07H6.6L1.75,40.12a.87.87,0,0,0,.47,1,.86.86,0,0,0,1.07-.31L20.46,15.07a.87.87,0,0,0-.72-1.34h-7L20.47,1.31A.83.83,0,0,0,20.49.44Z"/></g></g></svg>
              <h3 class="bold black">Description 4</h3>
              <p class="black">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.</p>
            </div>
          </div>
        </div>
      </div>   
    </div>
  </section>

  <section id="aboutfacility" class="uk-block-xlarge bg-white">
      <svg id="greensquiggle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621.65 454.14"><defs><style>.cls-1a{isolation:isolate;}.cls-2a{opacity:0.1;mix-blend-mode:multiply;}</style></defs><title>Asset 2</title><g class="cls-1a"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-2a"><path class="cls-3" d="M609,451c5.37-2.92,10.5-8.65,11.92-14.4,3-12.22-3.85-26.78-18-29.75a732.12,732.12,0,0,1-115.73-34.61,712.48,712.48,0,0,1-113.57-58c-9.9-6.25-19.64-12.72-29.13-19.51a237.14,237.14,0,0,1-38.88-35.86c-32.38-40.62-49.9-89.14-74.13-134-13-24.15-28.38-47.27-47.66-67.56a201.94,201.94,0,0,0-32.72-27.44c-11.9-8.14-24.81-14.27-38.41-19.53C83.57-.94,49.29-3.72,19.06,5.64c-6.39,2-12,5.53-15.48,11.05C.33,21.85-1.07,29.47.92,35.28c4.08,11.91,18,21.21,31.74,17a108.64,108.64,0,0,1,15-3.49,119.44,119.44,0,0,1,22,.17A143.3,143.3,0,0,1,96.56,56a164.44,164.44,0,0,1,29.65,16.39,189.09,189.09,0,0,1,29.27,28.17c44.63,56.07,60.87,127.07,105.81,183a249.45,249.45,0,0,0,44.14,43.38,618.57,618.57,0,0,0,51.86,35,710.68,710.68,0,0,0,102.63,51.5,765.63,765.63,0,0,0,129.19,39.95C595.85,454.81,603,454.32,609,451Z"/></g></g></g></g></svg>
      <svg id="bluesquiggle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 752.58 405.26"><defs><style>.cls-1b{isolation:isolate;}.cls-2b{opacity:0.1;mix-blend-mode:multiply;}.cls-3b{fill:#00953b;}</style></defs><title>Asset 1</title><g class="cls-1b"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-2b"><path class="cls-3b" d="M730.58,302.79c15.48-5.39,25.87-21.83,20.63-38.22A537.77,537.77,0,0,0,688,132a548.89,548.89,0,0,0-46-59.72c-15.78-17.93-33.29-33.78-53.31-46.88C569.87,13.1,547.78,4.24,525.39,1.52a149.13,149.13,0,0,0-22-1.49,146.31,146.31,0,0,0-22.16,2.73C469,5,457.19,10,446.34,16c-20.06,11-37.22,27.94-50.83,46.16-13.93,18.65-25.71,38.51-36.37,59.18-21.24,41.17-38.75,84.52-65.3,122.72A267.06,267.06,0,0,1,253,287.71a281.31,281.31,0,0,1-55.45,34.49,282.59,282.59,0,0,1-63.09,18.92,266.34,266.34,0,0,1-61,1.56,242.25,242.25,0,0,1-35.66-7.21c-15.47-4.42-34,6.76-37.1,22.58-3.34,16.87,6.07,32.39,22.59,37.11,73.32,20.93,153.48,8.15,220-27.47A294.47,294.47,0,0,0,326,301.91c24.81-28.57,43.28-61.79,59.86-95.6,17-34.62,32.51-70.52,54.61-102.29a175.24,175.24,0,0,1,24-26.44,113,113,0,0,1,17.32-11A93.74,93.74,0,0,1,496.89,62a102.29,102.29,0,0,1,17.76-.29,123.43,123.43,0,0,1,21.76,5.41,150.26,150.26,0,0,1,26.06,14.36c16.41,12.43,30.66,27.36,43.91,43.07a508.84,508.84,0,0,1,67,107.47,511.53,511.53,0,0,1,19,50.13,31.88,31.88,0,0,0,14.83,18.23C713.82,304,723.35,305.31,730.58,302.79Z"/></g></g></g></g></svg>
    <div class="gridl">
      <div class="uk-grid-small" uk-grid>
        <div id="about-left" class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
          <h2 class="black bold">What is the facility like?</h2>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
          <a href="#" class="green bold">See Our Facility</a>
          <div class="blackline"></div>
          <div class="review">
            <div class="uk-grid-small" uk-grid>
              <div class="uk-width-1-4 uk-text-center">
                <img class="avatar" src="https://journeypureriver.com/wp-content/uploads/2019/01/Cameron-100x100.png" alt="Review Avatar">
                <span class="black bold">Cameron B</span>
              </div>
              <div class="uk-width-3-4 uk-text-left">
                <img class="stars" src="/cpe/wp-content/uploads/2019/04/stars-gold.png" alt="Review Stars">
                <p class="small black">JourneyPure At The River gave me the tools and support I needed to live a life in sobriety. The community and sense of family is amazing.</p>
              </div>
            </div>
          </div>
        </div>
        <div id="about-right" class="uk-width-1-1 uk-width-1-2@m uk-text-center">
          <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-facility-overlapping-600x428.png" alt="JourneyPure CPE Facility">
        </div>
      </div>
    </div>
  </section>

  <section id="treatmentdescription" class="uk-block-xlarge bg-gray">
    <div class="gridl">
      <div class="uk-grid-small" uk-grid>
        <div id="treatment-left" class="uk-width-1-1- uk-width-2-3@m uk-text-center uk-text-left@m">
          <h2 class="black bold">What does treatment look like?</h2>
          <p class="black">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
        </div>
        <div id="treatment-right" class="uk-width-1-1 uk-width-1-3@m uk-text-center">
          <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-treatment-500x302.png">
        </div>
      </div>
    </div>
  </section>

  <section id="descriptionblurbs" class="uk-block-xlarge">
    <div class="gridl">
      <div class="uk-grid-medium" uk-grid>
        <div class="uk-width-1-1 uk-width-1-3@m">
          <div class="single">
            <div class="header bg-blue">
              <h3 class="white bold">Treating More Than Just Addiction</h3>
            </div>
            <div class="body bg-white">
              <p>When an individual is actively addicted, his or her mind, body, and heart become neglected, causing the individual to struggle with a number of effects. At JourneyPure Center for Professional Excellence, it is our clinical mission to put a person on the path to complete healing.</p>
              <a href="#" class="green">Learn More</a>
            </div>
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-3@m">
          <div class="single">
            <div class="header bg-blue">
              <h3 class="white bold">Educating Our Clients</h3>
            </div>
            <div class="body bg-white">
              <p>We believe that understanding the science of addiction is a critical aspect of the recovery process. We want our clients to have as much information as possible regarding addiction – in particular, the biological and genetic components, why it is difficult to break the cycle of addiction, and what to expect down the line emotionally, mentally, and physically as they continue in their recovery.</p>
              <a href="#" class="green">Learn More</a>
            </div>
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-3@m">
          <div class="single">
            <div class="header bg-blue">
              <h3 class="white bold">Continuing Care Plan</h3>
            </div>
            <div class="body bg-white">
              <p>When a client is participating in our
                  residential treatment program, our
                  staff is continually working as a
                  support system who supplies tools for
                  success in recovery, which includes
                  getting plenty of sleep and exercise.
                  However, when our clients finish their
                  residential treatment, they still need
                  support to achieve success in recovery.
                  This is why we offer continuing care
                  plans to all clients as they leave our
                  facility.</p>
              <a href="#" class="green">Learn More</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg id="blurb-bluesquiggle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 190 321.62"><defs><style>.cls-1{isolation:isolate;}.cls-2{opacity:0.1;mix-blend-mode:multiply;}.cls-3{fill:#0033a1;}</style></defs><title>Asset 5</title><g class="cls-1"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-2"><path class="cls-3" d="M131.66,23.48c3.58,19.81,6.67,24.91,7.94,63.21a220.08,220.08,0,0,1-5.45,49,232.12,232.12,0,0,1-19.66,50.08,232.79,232.79,0,0,1-32.36,43.57,219.24,219.24,0,0,1-39.56,31.08,197.86,197.86,0,0,1-27.11,12.8C3.06,277.91-3.66,294.4,2.07,306.4c6.09,12.79,20,18.41,33.19,13.39C94,297.52,140.68,249.61,167.11,193.29a242.35,242.35,0,0,0,22.24-84.16c2.32-31.1-1.84-62.13-7.53-92.64C176-14.74,128.25,4.62,131.66,23.48Z"/></g></g></g></g></svg>
  </section>

  <section id="providing" class="uk-block-xlarge bg-white">
      <svg id="providingsquiggle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 752.58 405.26"><defs><style>.cls-1c{isolation:isolate;}.cls-2c{opacity:0.1;mix-blend-mode:multiply;}.cls-3c{fill:#00953b;}</style></defs><title>Asset 1</title><g class="cls-1c"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-2c"><path class="cls-3c" d="M730.58,302.79c15.48-5.39,25.87-21.83,20.63-38.22A537.77,537.77,0,0,0,688,132a548.89,548.89,0,0,0-46-59.72c-15.78-17.93-33.29-33.78-53.31-46.88C569.87,13.1,547.78,4.24,525.39,1.52a149.13,149.13,0,0,0-22-1.49,146.31,146.31,0,0,0-22.16,2.73C469,5,457.19,10,446.34,16c-20.06,11-37.22,27.94-50.83,46.16-13.93,18.65-25.71,38.51-36.37,59.18-21.24,41.17-38.75,84.52-65.3,122.72A267.06,267.06,0,0,1,253,287.71a281.31,281.31,0,0,1-55.45,34.49,282.59,282.59,0,0,1-63.09,18.92,266.34,266.34,0,0,1-61,1.56,242.25,242.25,0,0,1-35.66-7.21c-15.47-4.42-34,6.76-37.1,22.58-3.34,16.87,6.07,32.39,22.59,37.11,73.32,20.93,153.48,8.15,220-27.47A294.47,294.47,0,0,0,326,301.91c24.81-28.57,43.28-61.79,59.86-95.6,17-34.62,32.51-70.52,54.61-102.29a175.24,175.24,0,0,1,24-26.44,113,113,0,0,1,17.32-11A93.74,93.74,0,0,1,496.89,62a102.29,102.29,0,0,1,17.76-.29,123.43,123.43,0,0,1,21.76,5.41,150.26,150.26,0,0,1,26.06,14.36c16.41,12.43,30.66,27.36,43.91,43.07a508.84,508.84,0,0,1,67,107.47,511.53,511.53,0,0,1,19,50.13,31.88,31.88,0,0,0,14.83,18.23C713.82,304,723.35,305.31,730.58,302.79Z"/></g></g></g></g></svg>
      
    <div class="gridl">
      <div class="uk-grid-small" uk-grid>
        <div id="providing-left" class="uk-width-1-1 uk-width-2-3@m uk-text-center uk-text-left@m">
          <h2 class="bold black">Who is providing treatment?</h2>
          <p class="black">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
              nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
              erat volutpat. Ut wisi enim ad minim veniam.
              </p>
          <a href="#" class="green">Learn more about our staff</a>
        </div>
        <div id="providing-right" class="uk-width-1-1 uk-width-1-3@m uk-text-center">
          <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-physicians-400x396.png" />          
        </div>
      </div>
    </div>
    <div class="blackline-center"></div>
    <div class="gridl blurbs">
      <div class="uk-grid-medium" uk-grid>
        <div class="uk-width-1-1 uk-width-1-3@m">
          <div class="single">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
            <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
            <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
            <a href="#" class="green">Learn More</a>
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-3@m">
          <div class="single">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
            <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
            <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
            <a href="#" class="green">Learn More</a>
          </div>
        </div>
        <div class="uk-width-1-1 uk-width-1-3@m">
          <div class="single">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
            <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
            <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
            <a href="#" class="green">Learn More</a>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
