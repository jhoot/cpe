@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <section id="postheader" class="uk-block-xlarge gridl uk-background-cover uk-text-center" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
      <div class="h1">
        <h1 class="white bold"><?php the_title(); ?></h1>
      </div>
    </section>

    <section id="thecontent" class="uk-block">
      <div class="gridm">
        <div class="header uk-text-center">
          <p class="bold black"><?php the_author(); ?></p>
          <p class="black"><?php the_date(); ?></p>
        </div>
        <?php the_content(); ?>
      </div>
    </section>
  @endwhile
@endsection
