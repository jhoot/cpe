{{--
  Template Name: Contact Template
--}}

@extends('layouts.app')

@section('content')
  <section id="contactfold" class="uk-block-xlarge bg-white">
    <div class="gridl">
      <div class="uk-grid-small" uk-grid>
        <div id="contactfold-left" class="uk-width-1-1 uk-width-2-5@m">
          <h1 class="bold black">Get in Touch</h1>
          <a href="#" class="green">(844) 259-9926</a>
          <p class="black">We are ready to speak with you 24/7. If you are calling about
            yourself or for a loved one, our admissions team is ready to
            answer your questions and get you the help you need,
            quickly and efficiently.</p>
          <p class="bold black">Email us: admits@journeypure.com</p>
          <p class="bold black">Chat with us: <a href="#" class="green bold">CHAT NOW</a> </p>
          <p class="bold black">5080 Florence Rd<br>Murfreesboro, TN 37129</p>
        </div>
        <div id="contactfold-right" class="uk-width-1-1 uk-width-3-5@m">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3230.808635182985!2d-86.4695911843834!3d35.92718382405034!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8864657855d08e9f%3A0xdab7a92753451289!2sExecutive+Addiction+Recovery+Center+(JourneyPure+Center+for+Professional+Excellence)!5e0!3m2!1sen!2sus!4v1555509135888!5m2!1sen!2sus" frameborder="0" style="position:absolute;top:0;left:0;width:100%;height:100%;" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </section>
@endsection