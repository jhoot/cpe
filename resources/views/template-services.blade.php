{{--
  Template Name: Services Template
--}}

@extends('layouts.app')

@section('content')
  <section id="locationfold" class="uk-block-xlarge bg-white">
    <div class="gridl">
      <div class="uk-grid-small" uk-grid>
        <div id="locationfold-left" class="uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
          <h1 class="bold black">We can show you the way.</h1>
          <p class="black">Lorem ipsum dolor sit amet, consectetuer
              adipiscing elit, sed diam nonummy nibh euismod
              tincidunt ut laoreet dolore magna aliquam
              erat volutpat. Ut wisi enim ad minim veniam.</p>
          <a href="#" class="button button-green">See Facility</a>
        </div>
        <div id="locationfold-right" class="uk-text-center uk-text-right@m uk-width-1-1 uk-width-1-2@m">
          <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-For-You-Overlay-500x500.png">
        </div>
      </div>
    </div>
  </section>

  <section id="services" class="bg-gray uk-block-xlarge">
    <div class="gridl">
      <div class="header uk-text-center">
        <h2 class="black bold">Our Services</h2>
      </div>
      <div class="uk-block">
          <div uk-slider="autoplay: true">
              <div class="uk-slider-container">
                  <ul class="uk-slider-items uk-text-center uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m">
                      <li>
                          <div class="single">
                            <img src="/cpe/wp-content/uploads/2019/04/Screenshot_1.png">
                            <div class="text uk-text-left">
                              <h3 class="bold black">Continuing Care Plan</h3>
                              <p class="black">When a client is participating in our
                                  residential treatment program, our
                                  staff is continually working as a
                                  support system who supplies tools for
                                  success in recovery, which includes
                                  getting plenty of sleep and exercise.
                                  However, when our clients finish their
                                  residential treatment, they still need
                                  support to achieve success in recovery.
                                  This is why we offer continuing care
                                  plans to all clients as they leave our
                                  facility.</p>
                            </div>
                          </div>
                      </li>
                      <li>
                        <div class="single">
                          <img src="/cpe/wp-content/uploads/2019/04/Screenshot_1.png">
                          <div class="text uk-text-left">
                            <h3 class="bold black">Continuing Care Plan</h3>
                            <p class="black">When a client is participating in our
                                residential treatment program, our
                                staff is continually working as a
                                support system who supplies tools for
                                success in recovery, which includes
                                getting plenty of sleep and exercise.
                                However, when our clients finish their
                                residential treatment, they still need
                                support to achieve success in recovery.
                                This is why we offer continuing care
                                plans to all clients as they leave our
                                facility.</p>
                          </div>
                        </div>
                    </li>
                    <li>
                      <div class="single">
                        <img src="/cpe/wp-content/uploads/2019/04/Screenshot_1.png">
                        <div class="text uk-text-left">
                          <h3 class="bold black">Continuing Care Plan</h3>
                          <p class="black">When a client is participating in our
                              residential treatment program, our
                              staff is continually working as a
                              support system who supplies tools for
                              success in recovery, which includes
                              getting plenty of sleep and exercise.
                              However, when our clients finish their
                              residential treatment, they still need
                              support to achieve success in recovery.
                              This is why we offer continuing care
                              plans to all clients as they leave our
                              facility.</p>
                        </div>
                      </div>
                  </li>
                  <li>
                    <div class="single">
                      <img src="/cpe/wp-content/uploads/2019/04/Screenshot_1.png">
                      <div class="text uk-text-left">
                        <h3 class="bold black">Continuing Care Plan</h3>
                        <p class="black">When a client is participating in our
                            residential treatment program, our
                            staff is continually working as a
                            support system who supplies tools for
                            success in recovery, which includes
                            getting plenty of sleep and exercise.
                            However, when our clients finish their
                            residential treatment, they still need
                            support to achieve success in recovery.
                            This is why we offer continuing care
                            plans to all clients as they leave our
                            facility.</p>
                      </div>
                    </div>
                </li>
                <li>
                  <div class="single">
                    <img src="/cpe/wp-content/uploads/2019/04/Screenshot_1.png">
                    <div class="text uk-text-left">
                      <h3 class="bold black">Continuing Care Plan</h3>
                      <p class="black">When a client is participating in our
                          residential treatment program, our
                          staff is continually working as a
                          support system who supplies tools for
                          success in recovery, which includes
                          getting plenty of sleep and exercise.
                          However, when our clients finish their
                          residential treatment, they still need
                          support to achieve success in recovery.
                          This is why we offer continuing care
                          plans to all clients as they leave our
                          facility.</p>
                    </div>
                  </div>
              </li>
              <li>
                <div class="single">
                  <img src="/cpe/wp-content/uploads/2019/04/Screenshot_1.png">
                  <div class="text uk-text-left">
                    <h3 class="bold black">Continuing Care Plan</h3>
                    <p class="black">When a client is participating in our
                        residential treatment program, our
                        staff is continually working as a
                        support system who supplies tools for
                        success in recovery, which includes
                        getting plenty of sleep and exercise.
                        However, when our clients finish their
                        residential treatment, they still need
                        support to achieve success in recovery.
                        This is why we offer continuing care
                        plans to all clients as they leave our
                        facility.</p>
                  </div>
                </div>
            </li>
                </ul>
              </div>
          </div>
      </div>
    </div>
  </section>

  <section id="committed" class="uk-block-xlarge bg-white">
    <div class="gridl">
      <div class="uk-grid-small" uk-grid>
        <div id="committed-left" class="uk-text-center uk-width-1-1 uk-width-2-5@m">
          <img src="/cpe/wp-content/uploads/2019/04/JP-CPE-Services-phone-265x500.jpg">
        </div>
        <div id="committed-right" class="uk-text-center uk-text-left@m uk-width-1-1 uk-width-3-5@m">
          <h2 class="black bold">We're committed to your long-term sobriety.</h2>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
              nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
              volutpat. Ut wisi enim ad minim veniam.</p>
              <div class="blackline-center-100"></div>
              <div class="uk-grid-small" uk-grid>
                <div class="uk-width-1-1 uk-width-1-2@m">
                  <div class="single">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
                    <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
                    <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
                    <a href="#" class="green">Learn More</a>
                  </div>
                </div>
                <div class="uk-width-1-1 uk-width-1-2@m">
                  <div class="single">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
                    <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
                    <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
                    <a href="#" class="green">Learn More</a>
                  </div>
                </div>
                <div class="uk-width-1-1 uk-width-1-2@m">
                  <div class="single">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.42 35.42"><defs><style>.cls-1{fill:#0033a1;}</style></defs><title>Asset 4</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M34.86.32a1.47,1.47,0,0,0-2.07.24L13.89,24.35,7.68,18.14A1.47,1.47,0,0,0,5.6,20.23L13,27.61A1.44,1.44,0,0,0,14,28h.08a1.51,1.51,0,0,0,1.08-.56L35.1,2.39A1.47,1.47,0,0,0,34.86.32Z"/><path class="cls-1" d="M27.38,16.26A1.48,1.48,0,0,0,26.27,18a12.26,12.26,0,0,1,.3,2.63A11.81,11.81,0,1,1,14.76,8.86,11.69,11.69,0,0,1,20,10.08a1.48,1.48,0,0,0,1.32-2.64A14.61,14.61,0,0,0,14.76,5.9,14.8,14.8,0,1,0,29.14,17.37,1.47,1.47,0,0,0,27.38,16.26Z"/></g></g></svg>
                    <h3 class="bold black">Lorem ipsum Lorem ipsum</h3>
                    <p class="black">Lorem ipsom dolor sit amet, Lorem.</p>
                    <a href="#" class="green">Learn More</a>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </div>
  </section>

@endsection